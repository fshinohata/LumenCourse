<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Rota principal (Página Inicial)
$app->get('/', [
	'as' => 'home',
	'uses' => 'HomeController@index'
]);

// Rota que leva para a página de criação de novas postagens
$app->get('/post/create', [
	'as' => 'post-create',
	'uses' => 'PostController@create'
]);

// Rota que leva ao método de criação REAL do PostController
$app->post('/post/create', [
	'as' => 'post-store',
	'uses' => 'PostController@store'
]);

// Rota que leva para a página de edição de uma postagem
$app->get('/post/edit/{id:[0-9]+}', [
	'as' => 'post-edit',
	'uses' => 'PostController@edit'
]);

// Rota que leva ao método de edição do PostController
$app->post('/post/edit/{id:[0-9]+}', [
	'as' => 'post-update',
	'uses' => 'PostController@update'
]);

// Rota que leva ao método de deleção do PostController
$app->post('/post/destroy/{id:[0-9]+}', [
	'as' => 'post-destroy',
	'uses' => 'PostController@destroy'
]);