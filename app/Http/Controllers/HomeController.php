<?php

namespace App\Http\Controllers;

use App\Post;

class HomeController extends Controller
{
    /**
     * Retorna a página inicial do website
     *
     * @return view
     */
    public function index()
    {
        // Pega todas as postagens do banco de dados
        $postagens = Post::get();

        // Retorna a página inicial
        return view('home.index')
            ->with([
                'PageTitle' => 'Página Inicial',
                'IntroMessage' => 'Olá! Bem-vindos ao curso de Lumen da ECOMP!',
                'posts' => $postagens
            ]);
    }
}
