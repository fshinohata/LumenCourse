<!DOCTYPE html>
<html>
<head>
    <title>{{ isset($PageTitle) ? $PageTitle : 'Curso de Lumen'}}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Saturn is free PSD &amp; HTML template by @flamekaizar">
    <meta name="author" content="Afnizar Nur Ghifari">

    @include('layout.css')
    @yield('css')
    @yield('top-js')

    <link rel="shortcut icon" href="/img/favicon.png" />
</head>
<body>
	<!-- Navigation -->
	@include('layout.navbar')

	<!-- Introduction -->
	<div class="intro">
		<div class="container">
			<div class="units-row">
			    <div class="unit-10">
			    	<img class="img-intro" src="/img/avatar.png" alt="">
			    </div>
			    <div class="unit-90">
			    	<p class="p-intro">{{ isset($IntroMessage) ? $IntroMessage : 'Olá! Bem-vindos ao curso de Lumen da ECOMP!'  }}</p>
			    </div>
			</div>
		</div>
	</div>
	
	<!-- Content -->
	<div class="content">
		<div class="container">
			@yield('content')
		</div>
	</div>

	@include('layout.footer')
	@include('layout.js')
	@yield('bottom-js')
</body>
</html>