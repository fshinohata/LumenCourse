<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    //------------------------------------------------------------------
    /*
        Funçao: Retorna página de criaçao de novos posts
        Retorno(s): View da página de criaçao de novos posts
     */
    public function create()
    {
        return view('posts.create');
    }



    //------------------------------------------------------------------
    /*
    	Funçao: Guarda a postagem no banco de dados.
    	Retorno(s): Redirecionamento para a ṕágina de visualização de postagens.
     */
    public function store(Request $request)
    {
    	// Regras para o título e o conteúdo serem válidos
    	$rules = [
            /* 
                Regras utilizadas aqui:
                - 'required' indica que não pode ser vazia!
                - 'max:<numero>' indica o limite de caracteres daquela variavel
             */
    		'post-title' => 'required',
    		'post-content' => 'required|max:65535'
    	];

    	// Mensagens de erro caso um dos requerimentos não seja satisfeito
    	$messages = [
    		'post-title.required' => 'Você precisa dar um título para a postagem!',
    		'post-content.required' => 'Sua postagem deve ter conteúdo!',
    		'post-content.max' => 'Sua postagem está acima do limite de 65535 caracteres!'
    	];

    	// Validação real
    	$validator = Validator::make($request->all(), $rules, $messages);

    	// Se houve alguma quebra de regra,
    	if($validator->fails())
    	{
            // Retorna a view de visualização de erros
            return view('validation-errors')->with(['errors' => $validator->errors()]);
    	}

    	// Se passou na validação, cria o post
    	Post::create([
    		'title' => $request['post-title'],
    		'content' => $request['post-content']
    	]);

    	// Retorna o usuário para a página de visualização de postagens
    	return redirect()->route('home');
    }



    //------------------------------------------------------------------
    /*
        Funçao: Retorna a view de edição de uma postagem indicada pelo parâmetro $id
        Retorno(s): View com a página de edição contendo os dados correspondentes
     */
    public function edit($id)
    {
        // Busca a postagem com ID indicado pela variável $id, recebida da rota
        $post = Post::find($id);

        // Se não encontrou a postagem, retorna para a página inicial
        if(!$post) return redirect()->route('home');

        // Retorna a view com os dados obtidos
        return view('posts.edit')->with(['post' => $post]);
    }



    //------------------------------------------------------------------
    /*
        Funçao: Atualiza uma postagem de ID indicado pelo parâmetro $id, recebido da rota, com os dados recebidos pela requisição $request do formulário
        Retorno(s): Redireciona para a página inicial, em todo caso
     */
    public function update(Request $request, $id)
    {
        // Busca a postagem com ID indicado pela variável $id, recebida da rota
        $post = Post::find($id);

        // Se não encontrou a postagem, retorna para a página inicial
        if(!$post) return redirect()->route('home');

        // Regras para o título e o conteúdo serem válidos
        $rules = [
            /* 
                Regras utilizadas aqui:
                - 'required' indica que não pode ser vazia!
                - 'max:<numero>' indica o limite de caracteres daquela variavel
             */
            'post-title' => 'required',
            'post-content' => 'required|max:65535'
        ];

        // Mensagens de erro caso um dos requerimentos não seja satisfeito
        $messages = [
            'post-title.required' => 'Você precisa dar um título para a postagem!',
            'post-content.required' => 'Sua postagem deve ter conteúdo!',
            'post-content.max' => 'Sua postagem está acima do limite de 65535 caracteres!'
        ];

        // Validação real
        $validator = Validator::make($request->all(), $rules, $messages);

        // Se houve alguma quebra de regra,
        if($validator->fails())
        {
            // Retorna a view de visualização de erros
            return view('validation-errors')->with(['errors' => $validator->errors()]);
        }

        // Altera o título da postagem com o recebido na requisição do formulário
        $post->title = $request['post-title'];

        // Altera o coneúdo da postagemcom o recebido na requisição do formulário
        $post->content = $request['post-content'];

        // Salva os novos dados de verdade, no banco de dados
        $post->save();

        // Redireciona o usuário de volta para a página inicial
        return redirect()->route('home');
    }



    //------------------------------------------------------------------
    /*
        Funçao: Apaga a postagem de ID indicado por $id do banco de dados
        Retorno(s): Redireciona o usuário de volta para a página inicial, em todo caso.
     */
    public function destroy($id)
    {
        // Busca a postagem no banco de dados
        $post = Post::find($id);

        // Se não existe, não deleta! (e redireciona o usuário para a página inicial)
        if(!$post) return redirect()->route('home');

        // Apaga do banco de dados
        $post->delete();

        // Redireciona o usuário para a página inicial
        return redirect()->route('home');
    }
}
