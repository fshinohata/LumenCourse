@extends('layout.body')

@section('css')
	{{-- Adicionando bootstrap --}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

	{{-- CSS desta página --}}
	<style>
		textarea {
			resize:none;
			height:30vh;
		}
	</style>
@endsection

@section('content')
	{{-- Mensagens de erro --}}
	@if(isset($errors) && count($errors))
	<div class="row">
		<div class="col-lg-12">
			<div class="alert alert-danger">
				<h4>Atenção!</h4>
				<ul>
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	@endif

	{{-- Formulário de criaçao de um novo post --}}
	<form method="POST" action="{{ route('post-update', ['id' => $post->id]) }}">
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label for="post-title">Título do Post:</label>
					<input type="text" class="form-control" name="post-title" placeholder="Seu título aqui" value="{{ $post->title }}">
				</div>
				<div class="form-group">
					<label for="post-content">Conteúdo do Post:</label>
					<textarea type="text" class="form-control" name="post-content" placeholder="Seu conteúdo aqui" spellcheck="false">{{ $post->content }}</textarea>
				</div>
				<button class="btn btn-success" type="submit"> Salvar</button>
				<a href="{{ route('home') }}" class="btn btn-danger"> Cancelar</a>
			</div>
		</div>
	</form>
@endsection