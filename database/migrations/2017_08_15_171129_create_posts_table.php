<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Criação da tabela. A função up() é a responsável por criar/alterar as tabelas!
        Schema::create('posts', function(Blueprint $table) {
            // Coluna de auto-incremento chamada ID, com atributo de ÚNICO (PRIMARY KEY)
            $table->increments('id')->unique();

            // Coluna chamada TITLE, que conterá uma string para guardar o título da postagem
            $table->string('title');

            // Coluna chamada CONTENT, que conterá uma string grande (texto) para guardar o conteúdo da postagem
            $table->text('content');

            // Colunas de "created_at" e "updated_at", exigidas pelo Lumen por padrão
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Reverte os comandos da função up(). Se você criou a tabela, aqui você deve deletá-la!
        Schema::dropIfExists('posts');
    }
}
