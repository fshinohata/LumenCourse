@extends('layout.body')

@section('css')
	{{-- Adicionando bootstrap --}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
@endsection

@section('content')
	@if(count($posts))
		@foreach($posts as $post)
			<div class="post">
				<!-- Heading -->
				<a href="#"><h1>{{ $post->title }}</h1></a>
				<hr>
				<div class="in-content">
					<pre>{{ $post->content }}</pre>
					<a class="read-more" href="{{ route('post-edit', ['id' => $post->id]) }}">Editar</a>
					<a class="read-more" href="#" onclick="event.preventDefault(); document.getElementById('delete{{ $post->id }}').submit()">Deletar</a>
					<form id="delete{{ $post->id }}" method="POST" action="{{ route('post-destroy', ['id' => $post->id]) }}">
					</form>
				</div>
			</div>
		@endforeach
	@endif
@endsection