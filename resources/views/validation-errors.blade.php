<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<title>{{ isset($PageTitle) ? $PageTitle : 'Curso de Lumen'}}</title>
	<meta charset="utf-8">

	{{-- Adicionando bootstrap --}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

	<style>
		body {
			margin: 50px;
		}

		.align-text-center {
			text-align: center;
		}

		button:hover {
			cursor:pointer;
		}
	</style>
</head>
<body>
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">
			<h4>Atenção!</h4>
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="align-text-center">
			<button type="button" class="btn btn-danger" onclick="redirect_back()">Voltar</button>
		</div>
	</div>
</div>

<script>
	function redirect_back() { history.back(); }
</script>
</body>
</html>