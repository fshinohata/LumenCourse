<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
	// Nome da tabela (necessário apenas em alguns casos)
    protected $table = 'usuarios';

    // Dados alteráveis da tabela
    protected $fillable = [
    	'nome',
    	'email',
    	'senha',
    ];

    // Dados não alteráveis da tabela
    protected $guarded = [
    	'id',
    ];
}
