<div class="main-nav">
	<div class="container">
		<header class="group top-nav">
			<nav class="navbar logo-w navbar-left" >
				<a class="logo" href="{{ route('home') }}">ECOMP</a>
			</nav>
			<div class="navigation-toggle" data-tools="navigation-toggle" data-target="#navbar-1">
			    <span class="logo">ECOMP</span>
			</div>
		    <nav id="navbar-1" class="navbar item-nav navbar-right">
			    <ul>
			        <li><a href="{{ route('home') }}">Página Inicial</a></li>
			        <li><a href="{{ route('post-create') }}">Criar Postagem</a></li>
			    </ul>
			</nav>
		</header>
	</div>
</div>