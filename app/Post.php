<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	// Colunas alteráveis por JavaScript
    protected $fillable = [
    	'title',
    	'content',
    ];

    // 
    protected $hidden = [
    	'id',
    ];

    protected $guarded = [
    	'created_at',
    	'updated_at',
    ];
}
